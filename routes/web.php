<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/hello', function (){
    return "hello world";
});

Route::get('/customers/{id?}', function($id='No customer was provided'){
    if($id == 'No customer was provided')
        return view('nocustomers', ['id' => $id]);
    else
        return view('customers', ['id' => $id]);
})->name('customers');

Route::resource('todos', 'TodoController'); 